import React from 'react'

import './BurgerMenu.scss'

const BurgerMenu = ({ isOpen, ...props }) => (
	<div className="menu-wrapper" {...props}>
		<div className={isOpen ? "burger-menu burger-menu--open" : "burger-menu"} />
	</div>
);

export default BurgerMenu;
