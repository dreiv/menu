import React from 'react'

import { LayoutConsumer } from '../../Layout'
import BurgerMenu from './burger-menu/BurgerMenu'

import './NavToggler.scss'

const NavToggler = () => (
	<LayoutConsumer>
		{({ expanded, actions }) => (
			<button
				className="nav-toggler"
				onClick={actions.handleNavToggle}
			>
				<BurgerMenu isOpen={expanded} />
				<span className="nav-toggler-menu">Menu</span>
			</button>
		)}
	</LayoutConsumer>
)

export default NavToggler