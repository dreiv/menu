import React, { Component } from 'react'

import NavToggler from './nav-toggler'

import './Header.scss'

const isScrolled = () => document.documentElement.scrollTop !== 0

class Header extends Component {
	state = {
		isScrolled: isScrolled()
	}

	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll)
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll)
	}

	handleScroll = () => {
		const isScrolledChanged = isScrolled()

		this.setState(prevState => {
			if (prevState.isScrolled !== isScrolledChanged) {
				return { isScrolled: isScrolledChanged }
			}

			return null
		})
	}

	render() {
		const { isScrolled } = this.state

		return (
			<div className={isScrolled ? 'header header--scrolled' : 'header'}>
				<NavToggler />
				<div>🔍 <input type="search" placeholder="Search..."/></div>
				<div>⌄😎</div>
			</div>
		)
	}
}

export default Header
