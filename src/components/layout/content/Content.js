import React from 'react'

import './Content.scss'

const Content = () => (
	<main className='content'>
		<div>
			Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi aut ab harum inventore, incidunt eum repellat obcaecati enim quis labore quos quam accusamus rem corporis mollitia. Harum beatae laudantium praesentium!
			Placeat mollitia minus, est nobis consequuntur vel cumque magni neque possimus perferendis voluptatum dicta fugiat ab sed ullam excepturi dolorem, amet porro! Perferendis sint natus voluptates porro molestias itaque aliquam?
			Doloribus error unde reiciendis nobis a, ipsa dolorum fugit fugiat iure pariatur, ea reprehenderit eligendi ipsum modi. Iure ipsam accusamus amet rem commodi cum, magnam, porro sed quis repellat aperiam.
			Placeat laudantium sapiente et possimus. Ea veritatis, aut, fugit obcaecati dolore architecto a ab eaque facere consectetur, ipsa nihil enim beatae qui inventore dolorum nobis deserunt fugiat? Inventore, eveniet nobis!
			Fugit id aperiam atque placeat magni officiis error ipsa. Perspiciatis nostrum tempore delectus error pariatur accusamus a, doloribus debitis iusto molestias praesentium officia illum aliquid quod deserunt exercitationem iure nobis!
			Quis illo, alias est facilis perferendis repellat! Commodi cum perferendis fugit ullam dolorem nulla deserunt nesciunt quos beatae? Et molestias, a repudiandae officiis sapiente quidem doloremque rem reiciendis tempore non!
			Quasi illum dignissimos vitae provident est ea asperiores repellat vel voluptates qui maiores delectus ut, debitis modi impedit possimus! Magnam molestiae ratione eligendi sint vero ipsum. Voluptates excepturi amet ea.
			Quo numquam voluptatem cum soluta, similique sequi. Ad placeat id itaque ullam iure temporibus laudantium vero similique? Cum iusto est excepturi, unde dolorem voluptas, voluptatum reprehenderit eveniet consectetur cumque cupiditate.
			Molestias est illum deleniti veritatis. Nesciunt, consequuntur sed sapiente quos unde ipsa perspiciatis consequatur fugiat adipisci temporibus quaerat magnam nulla. Placeat, nobis distinctio facere iusto temporibus laboriosam earum totam consequatur!
			Exercitationem, similique aliquam! Sunt culpa tenetur eum doloremque, reprehenderit assumenda quis ullam quisquam, alias est laboriosam at maiores eaque vero. Amet cupiditate blanditiis asperiores ab ut adipisci nesciunt dolore natus.
			Fugit voluptate eveniet harum dolorem repellat corporis velit aliquid. Hic voluptatibus quis in perspiciatis ut soluta quas, explicabo maxime vero praesentium molestias fugit possimus magnam, vitae porro? Dolorum, perferendis rem.
			Id excepturi corrupti perspiciatis neque officia necessitatibus dignissimos nostrum deserunt voluptatem cumque, facilis provident ad. Quis voluptates molestiae, alias blanditiis, enim necessitatibus eligendi nobis totam, suscipit accusamus itaque explicabo a.
			Temporibus iusto consectetur tenetur molestiae corporis similique nobis cum libero inventore consequatur illum sunt tempora qui, nemo magnam earum quod modi dolores ipsam est odio quis? Expedita excepturi error necessitatibus.
			A molestiae eius rem aperiam earum. Facilis, mollitia nisi tempora dolorem blanditiis, dolor harum veritatis commodi consequuntur soluta tempore suscipit nostrum ducimus veniam sed facere corrupti reiciendis modi iure maiores?
			Nulla explicabo praesentium quaerat voluptatibus debitis tenetur molestias, sint veritatis! Sit maiores placeat voluptas, maxime hic provident a in explicabo accusamus aliquam, qui ipsa, mollitia nisi dolore laudantium nobis sapiente.
			Cum possimus minus ducimus similique, at voluptatem. Magni excepturi repellat nemo doloribus quae minima quisquam consectetur odio eaque, quaerat expedita ut tenetur labore quos suscipit molestias quas magnam, fugiat consequuntur!
			Dicta, eos unde aliquid deserunt modi tempora sed corrupti earum, exercitationem iusto in voluptas quaerat repellendus. Architecto, quas, iste, eum voluptatibus debitis odio expedita consectetur adipisci unde itaque atque fugiat.
			Eligendi, harum! Pariatur distinctio laborum, velit ad deserunt est vel sapiente neque, sunt nisi ipsa. Dicta tempore officia perspiciatis odit recusandae labore. Eligendi sed non earum veniam officiis perferendis maiores?
			Odit fugiat, deleniti in illo modi placeat ullam laudantium rerum repudiandae, delectus sequi ut numquam cumque iste omnis commodi fugit suscipit dolorem fuga? Eveniet quidem, saepe est culpa odio repudiandae!
			Iusto tempora optio voluptatum, consequatur natus est. Reiciendis, ullam quisquam quos sit quis pariatur voluptas rerum dignissimos quidem explicabo reprehenderit dicta aliquam enim ipsa sequi qui est voluptatibus saepe doloremque!
			Nam, sint, magnam nesciunt et aut obcaecati libero aliquid quam illum dolore laudantium. Corrupti doloribus qui quo commodi. Modi neque molestiae fugiat laborum odio nam? Dignissimos necessitatibus dolorem vero dicta.
			Exercitationem voluptatibus officiis consequuntur error nisi possimus molestiae reiciendis tempora repellat aspernatur! Itaque quos voluptas omnis, alias fugit quisquam tenetur illum beatae necessitatibus saepe, animi officiis error nihil perspiciatis doloribus?
			Corporis, laborum? Nam, id. Odio dolorem modi quaerat veniam quae ipsa accusamus ad, inventore rerum exercitationem harum dolorum ipsam explicabo sequi laudantium cum ullam unde deleniti qui quidem aliquid facilis.
			Facilis labore impedit veritatis obcaecati culpa voluptatibus corrupti laudantium amet sapiente consectetur, distinctio, sequi unde, consequuntur libero eum quasi numquam fuga natus ab quos dolorum dignissimos! Odit culpa temporibus ex!
			Quidem molestiae quasi doloremque, dolorum voluptatem deserunt. Iusto, pariatur cumque, fuga soluta explicabo provident eaque excepturi ab dolores laboriosam aperiam accusantium labore eum similique impedit nihil officiis velit mollitia asperiores!
			Accusantium tempore doloremque officia ut expedita voluptatum perspiciatis illo quidem, deleniti, pariatur dolor asperiores consequuntur aspernatur incidunt! Nesciunt sequi deserunt quia numquam voluptates accusamus quod harum magnam excepturi! Sequi, accusantium!
			Quisquam vitae ullam aspernatur, soluta nam expedita eos voluptatem libero esse sint ipsum eum dolorum molestiae atque dignissimos consequuntur ipsa labore. Odit perferendis delectus iure officia repellat voluptatum ad asperiores.
			Pariatur, explicabo, cupiditate aut blanditiis nobis aperiam sunt, voluptate officiis dolore id nihil sed. Beatae aliquam obcaecati soluta quae adipisci quaerat excepturi nam ipsa accusamus impedit, exercitationem eligendi laudantium a?
		</div>
	</main>
)

export default Content
