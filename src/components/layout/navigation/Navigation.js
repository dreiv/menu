import React from 'react'

import { LayoutConsumer } from '../Layout'

import './Navigation.scss'

const Navigation = () => (
	<LayoutConsumer>
		{({ expanded, actions }) => (
			<>
				<div
					className={expanded ? 'phone-backdrop' : null}
					onClick={actions.handleNavClose}
				/>
				<nav className={expanded ? 'nav nav-expanded' : 'nav'}>
					<div className="nav-header">
						Menu
						<button className="close-btn" onClick={actions.handleNavClose}>
							&times;
						</button>
					</div>

					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Products</a></li>
						<li><a href="#">Services</a></li>
						<li><a href="#">Shop</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</nav>
			</>
		)}
	</LayoutConsumer>
)

export default Navigation
