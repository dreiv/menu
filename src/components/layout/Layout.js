import React, { Component } from 'react'

import Content from './content'
import Header from './header'
import Navigation from './navigation'

import './Layout.scss'

const LayoutContext = React.createContext()
const LayoutProvider = LayoutContext.Provider
export const LayoutConsumer = LayoutContext.Consumer

class Layout extends Component {
	state = {
		expanded: false
	}

	handleNavToggle = () => {
		this.setState(prevState => {
			const expanded = !prevState.expanded
			document.body.classList.toggle('phone-prevent-scroll', expanded)

			return ({ expanded })
		})
	}

	handleNavClose = () => {
		document.body.classList.remove('phone-prevent-scroll')
		this.setState({ expanded: false })
	}

	render() {
		const { expanded } = this.state
		const context = {
			expanded,
			actions: {
				handleNavToggle: this.handleNavToggle,
				handleNavClose: this.handleNavClose
			}
		}

		return (
				<LayoutProvider value={context}>
					<Header />
					<Content />
					<Navigation />
				</LayoutProvider>
		)
	}
}

export default Layout
